*Creeper* creeps on [Krastorio 2](https://mods.factorio.com/mod/Krastorio2) by letting the creep creep. Pollution drives the creeping, but only via the spawner connected to it. The more active the spawner, the more active the creep; [Rampant](https://mods.factorio.com/mod/Rampant) death worlds beware! Creep without a spawner feeding it lay dorment, and those that are creeping, creep mostly toward heavier polluted sectors.

Creep also contributes to the evolution factor, which can be inspected with the console command `/creepolution`. The amount of growth and the evolution factor can be configured in the runtime (map) settings.

Fun fact: The creep in Krastorio absorbs a metric ton of pollution, literally figuratively. As creep expands, there will be less pollution absorbed by spawners, which means less creep creeping (and less attacks, but we're here for the creep).
